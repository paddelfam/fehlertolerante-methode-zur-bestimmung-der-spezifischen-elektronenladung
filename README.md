Die Bestimmung der spezifischen Elektronenladung ist ein klassischer Versuch im Physikunterricht der gymnasialen Oberstufe und im Studium verschiedenster Naturwissenschaften. Lehrmittelhersteller bieten für diesen Versuch fertig aufgebaute Versuchsanordnungen an, die alle ein Fadenstrahlrohr verwenden, das sich innerhalb einer Helmholtzspule befindet. Dieser Versuchsaufbau ist in der Fachliteratur auch als Schuster-Methode bekannt, benannt nach dem britischen Physiker Arthur Schuster (1). Bedingt durch den Aufbau des Fadenstrahlrohres ergeben sich für die Bestimmung der spezifischen Elektronenladung recht große Abweichungen zum theoretischen Wert. Dies liegt im Wesentlichen an der Verwendung einer mit einem Edelgas gefüllten Röhre und einem unscharfen Elektronenstrahl. Je nach gewähltem Hersteller liegt diese Abweichung bei ca. 50% bis hin zu einer Zehnerpotenz (2). 
Daher wird an technischen Hochschulen und Universitäten vereinzelt auch eine genauere Methode verwendet (3), (4), (5). Dieses Verfahren geht auf den deutschen Physiker Hans Busch (6) zurück und nutzt dazu eine Braunsche Röhre in einer langen Magnetspule, in der ein transversales elektrisches Feld und ein longitudinales Magnetfeld zu einer spiralförmigen Bahn der Elektronen führen. Leider ist eine Überprüfung der Homogenität des Magnetfeldes in der langen Magnetspule, aufgrund der räumlichen Enge, nur sehr schwierig möglich. Auch der Einfluss der metallischen Konstruktion und der elektrischen Ströme innerhalb der Braunschen Röhre auf das umgebene Magnetfeld lässt sich bei dieser Methode nicht bestimmen.
Wir haben deshalb eine Methode entwickelt, bei der wir eine kompakte Kathodenstrahlröhre verwenden, die sich komplett innerhalb eines homogenen Magnetfeldes einer entsprechend großen Helmholtzspule befindet. Der große „Bauraum“ der Helmholtzspule erlaubt dabei, während des Betriebes der Kathodenstrahlröhre, Messungen des Magnetfeldes durchzuführen. Um jederzeit die Funktion aller verwendeten Bauteile kontrollieren zu können, haben wir diese alle selbst entworfen und aufgebaut. So konnten wir die Größe der Helmholtzspule passend zur verwendeten Kathodenstrahlröhre konstruieren. Für die Ansteuerung der Kathodenstrahlröhre haben wir, im Gegensatz zu den bekannten Versuchsaufbauten verschiedener Universitäten, keine fertige Elektronik eines Oszillographen benutzt, sondern alle elektronischen Komponenten hierfür selbst entwickelt und aufgebaut. Dabei konnten wir ein tiefgehendes Verständnis für unsere Messmethode entwickeln. 
Unsere Messmethode erforderte eine hohe Genauigkeit an die mechanische Konstruktion der verschiedenen Messaufbauten. Dabei musste wegen der Magnetfelder auf metallische Komponenten unbedingt verzichtet werden. Wir realisierten daher den gesamten mechanischen Aufbau mit Hilfe von Kunststoffkomponenten. Diese haben wir mit Hilfe einer 3D-Konstruktionssoftware selbst entworfen und unter Verwendung eines 3D-Druckers hergestellt. 








1.	https://gpr.physik.hu-berlin.de/Aktuelles/E12%20neu.pdf 
2.	https://www.physikalische-schulexperimente.de/physo/Zwei_Fadenstrahlröhren_im_Vergleich 
3.	www.martin-wolf.org/physics/basicpractraining/A9/A9_Protokoll.pdf 
4.	https://www.physik.uzh.ch/~matthias/espace-assistant/manuals/de/Anleitung-PHY122.pdf 
5.	Hans Busch „Eine neue Methode zur e/m-Bestimmung (Vorläufige Mitteilung)“, Physikalische Zeitschrift, Bd. 23, 1922, S. 438-441 
6.	https://de.wikipedia.org/wiki/Hans_Busch_(Physiker)
