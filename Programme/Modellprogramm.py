import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import messagebox
import math
import os
import mpl_toolkits.mplot3d.art3d as art3d
from matplotlib.patches import Circle
import numpy as np
from PIL import Image


class Window(object):
    def __init__(self):
        self.lastNumbers = []
        self.MassElectron = float(9.10938188 * math.pow(10, -31))
        self.LadungElectron = float(1.602176462 * math.pow(10, -19))
        self.pi = float(3.1415926535897932384626433832)
        self.magnetischeFeldkonstante = float(4 * self.pi * math.pow(10, -7))
        self.Teilung = 100
        self.refreshed = False
        self.image_path = "{}/resources/plot.png".format(os.getcwd())
        self.limits = None
        self.scaleInUse = False
        self.s = None
        self.l = None
        self.d = None
        self.n = None
        self.r = None

        self.window = tk.Tk()
        self.window.title("Simulation")
        self.window.protocol("WM_DELETE_WINDOW", self.askquit)
        self.xWin = int(self.window.winfo_screenwidth() - 16)
        yWin = int(self.window.winfo_screenheight() - 40)
        self.window.geometry("{}x{}+{}+{}".format(self.xWin, yWin, 0, 0))
        self.window.resizable(width=False, height=False)

        self.canvas = tk.Canvas(self.window, width=self.xWin, height=yWin)
        self.image_canvas = tk.Canvas(self.window, width=1000, height=1000)

        self.canvas.place(x=0, y=0)
        self.image_canvas.place(x=800, y=30)

        self.fig = plt.figure(figsize=(8, 8))

        # self.window.bind("<Button-1>", self.callback)
        self.window.bind("<ButtonPress-1>", self.press)
        self.window.bind("<ButtonRelease-1>", self.release)
        self.setup()
        self.Teilung = int(833 / self.sVar.get())
        self.update()

        self.window.mainloop()

    def callback(self, event):
        print("x: {} | y: {}".format(event.x, event.y))

    def phaseCalc(self, T, t):
        phi = float(((self.pi * 2) / T) * t)
        while phi > float(self.pi * 2):
            phi = float(phi - (self.pi * 2))
        return phi

    def MagnetischeFlussdichteDerSpulen(self):
        x = float((self.magnetischeFeldkonstante * self.n * float(self.ISpuleVar.get()) * self.r) /
                  float(math.pow((math.pow((self.r / 2), 2) + math.pow(self.r, 2)), (2 / 3))))
        return x

    def TCalc(self, B):
        T = float((2 * self.MassElectron * self.pi) / (self.LadungElectron * B))
        return T

    def Berechnungen(self):
        xs = []
        ys = []  # <-- That's the height
        zs = []

        B = float(self.MagnetischeFlussdichteDerSpulen() * 5)
        # print(B)
        vx = float(math.sqrt(float((2 * self.LadungElectron * float(self.UBeschleunigungVar.get())) /
                                   self.MassElectron)))
        tBisSchirm = float(self.s / vx)
        tParts = float(tBisSchirm / self.Teilung)
        if not B == 0:
            T = self.TCalc(B)
            # print("T = {}".format(T))
            RadiusFlugbahn = float(((self.l * float(self.UKondensatorVar.get())) / (self.d * B)) *
                                   math.sqrt(self.MassElectron / (2 * self.LadungElectron *
                                                                  float(self.UBeschleunigungVar.get()))))
            # print("Radius: {}".format(RadiusFlugbahn))
            for zahl in range(int(self.Teilung + 1)):
                t = float(tParts * zahl)
                phi = self.phaseCalc(T, t)
                ys.append(float(RadiusFlugbahn * math.sin(phi)))
                zs.append(float(RadiusFlugbahn * math.cos(phi) - RadiusFlugbahn))
        else:
            for b in range(int(self.Teilung + 1)):
                ys.append(0)
                zs.append(0)
        for a in range(int(self.Teilung + 1)):
            t = float(tParts * a)
            xs.append(float(t * vx))
        return xs, ys, zs

    def returnMinMax(self, array):
        minx = 0
        maxx = 0
        for element in array:
            if element < minx:
                minx = element
            elif element > maxx:
                maxx = element
        return [minx, maxx]

    def update(self):
        # recentNumbers = [ISpule, UBeschleunigung, UKondensator, angle, elevation] -> Only scales get looked over
        if not self.scaleInUse:
            recentNumbers = [float(self.ISpuleVar.get()), float(self.UBeschleunigungVar.get()),
                             float(self.UKondensatorVar.get()), self.angleVar.get(), self.elevationVar.get()]
        else:
            recentNumbers = self.lastNumbers
        if not self.lastNumbers == recentNumbers or self.refreshed:
            if self.refreshed:
                self.s = float(self.sVar.get())
                self.l = float(self.lVar.get())
                self.d = float(self.dVar.get())
                self.n = int(self.nVar.get())
                self.r = float(self.rVar.get())
                self.refreshed = False
            xs, ys, zs = self.Berechnungen()
            ax = self.fig.add_subplot(111, projection="3d")

            ax.autoscale(enable=False, axis='both')
            if self.limits is None:
                # erweiterung = 0.05
                self.limits = [[0, self.sVar.get()], self.returnMinMax(zs), self.returnMinMax(ys)]
                # self.limits[1][0] -= erweiterung
                # self.limits[1][1] += erweiterung
                # self.limits[2][0] -= erweiterung
                # self.limits[2][1] += erweiterung
            ax.set_xlim(self.limits[0])
            ax.set_ylim([-0.0075, 0.0075])
            ax.set_zlim([-self.limits[2][1], self.limits[2][1]])
            ax.set_xlabel("Strecke s")
            ax.set_ylabel("y in m")
            ax.set_zlabel("z in m")

            negys = np.zeros(shape=np.asarray(ys).shape)
            negzs = np.zeros(shape=np.asarray(zs).shape)
            for x in range(len(ys)):
                negys[x] = -ys[x]
                negzs[x] = -zs[x]

            ax.scatter3D(xs, zs, ys, depthshade=False, c='blue')
            # ax.scatter3D(xs, negzs, negys, depthshade=False, c="yellow")

            p = Circle((0, 0), self.limits[2][1] * 1.5, alpha=0.25, color='green')
            ax.add_patch(p)
            art3d.pathpatch_2d_to_3d(p, z=self.sVar.get(), zdir='x')

            ax.view_init(self.elevationVar.get(), self.angleVar.get())

            try:
                img = Image.open(self.image_path)
                image = np.array(img)
                for x in range(len(image)):
                    for y in range(len(image[x])):
                        image[x][y] = 255
                Image.fromarray(image).save(self.image_path, format="png")
            except:
                raise Exception("Could not reset image.")

            plt.savefig(self.image_path, format='png')
            img = tk.PhotoImage(file=self.image_path)
            try:
                self.image_canvas.delete(self.id)
            except:
                pass
            self.window.img = img
            self.id = self.image_canvas.create_image((img.width() // 2, img.height() // 2), anchor=tk.CENTER, image=img)

            self.lastNumbers = recentNumbers
        self.canvas.after(100, self.update)

    def askquit(self):
        if messagebox.askyesno("Quit", "Do you want to quit?"):
            quit()

    def press(self, _):
        self.scaleInUse = True

    def release(self, _):
        self.scaleInUse = False

    def refresh(self):
        self.refreshed = True

    def setup(self):
        self.label = tk.Label(self.canvas, text="Parameter", font=("arial", 14))

        self.sVar = tk.DoubleVar()
        self.sLabel = tk.Label(self.canvas, text="s in m", font=("arial", 11))
        self.sEntry = tk.Entry(self.canvas, textvariable=self.sVar)
        self.sVar.set(0.12)
        self.s = float(self.sVar.get())

        self.lVar = tk.DoubleVar()
        self.lLabel = tk.Label(self.canvas, text="l in m", font=("arial", 11))
        self.lEntry = tk.Entry(self.canvas, textvariable=self.lVar)
        self.lVar.set(0.03)
        self.l = float(self.lVar.get())

        self.dVar = tk.DoubleVar()
        self.dLabel = tk.Label(self.canvas, text="d in m", font=("arial", 11))
        self.dEntry = tk.Entry(self.canvas, textvariable=self.dVar)
        self.dVar.set(0.004)
        self.d = float(self.dVar.get())

        self.rVar = tk.DoubleVar()
        self.rLabel = tk.Label(self.canvas, text="Spulenradius in m", font=("arial", 11))
        self.rEntry = tk.Entry(self.canvas, textvariable=self.rVar)
        self.rVar.set(0.15)
        self.r = float(self.rVar.get())

        self.nVar = tk.IntVar()
        self.nLabel = tk.Label(self.canvas, text="n", font=("arial", 11))
        self.nEntry = tk.Entry(self.canvas, textvariable=self.nVar)
        self.nVar.set(230)
        self.n = int(self.nVar.get())

        self.aktuButton = tk.Button(self.canvas, text="Apply changes", font=("arial", 11), command=self.refresh)

        self.ISpuleVar = tk.DoubleVar()
        self.ISpuleLabel = tk.Label(self.canvas, text="Spulenstrom in A", font=("arial", 11))
        self.ISpuleSR = tk.Scale(self.canvas, from_=0, to=3, orient=tk.HORIZONTAL, length=500, resolution=0.01,
                                 variable=self.ISpuleVar)
        self.ISpuleEntry = tk.Entry(self.canvas, textvariable=self.ISpuleVar)
        self.ISpuleVar.set(2.38)

        self.UBeschleunigungVar = tk.DoubleVar()
        self.UBeschleunigungLabel = tk.Label(self.canvas, text="Beschleunigungsspannung in V", font=("arial", 11))
        self.UBeschleunigungScale = tk.Scale(self.canvas, from_=50, to=2000, orient=tk.HORIZONTAL, length=500,
                                             variable=self.UBeschleunigungVar)
        self.UBeschleunigungEntry = tk.Entry(self.canvas, textvariable=self.UBeschleunigungVar)
        self.UBeschleunigungVar.set(1000)

        self.UKondensatorVar = tk.DoubleVar()
        self.UKondensatorLabel = tk.Label(self.canvas, text="Kondensatorspannung in V", font=("arial", 11))
        self.UKondensatorScale = tk.Scale(self.canvas, from_=0, to=100, orient=tk.HORIZONTAL, length=500,
                                          resolution=0.1,
                                          variable=self.UKondensatorVar)
        self.UKondensatorEntry = tk.Entry(self.canvas, textvariable=self.UKondensatorVar)
        self.UKondensatorVar.set(50)

        self.angleVar = tk.IntVar()
        self.angleLabel = tk.Label(self.canvas, text="Angle of view", font=("arial", 11))
        self.angleScale = tk.Scale(self.canvas, from_=0, to=180, orient=tk.HORIZONTAL, length=180, resolution=1,
                                   variable=self.angleVar)
        self.angleVar.set(120)

        self.elevationVar = tk.IntVar()
        self.elevationLabel = tk.Label(self.canvas, text="Elevation of view", font=("arial", 11))
        self.elevationScale = tk.Scale(self.canvas, from_=0, to=90, orient=tk.HORIZONTAL, length=90, resolution=1,
                                       variable=self.elevationVar)
        self.elevationVar.set(20)

        self.label.pack()
        self.sLabel.pack()
        self.sEntry.pack()
        self.lLabel.pack()
        self.lEntry.pack()
        self.dLabel.pack()
        self.dEntry.pack()
        self.rLabel.pack()
        self.rEntry.pack()
        self.nLabel.pack()
        self.nEntry.pack()
        self.aktuButton.pack()
        self.ISpuleLabel.pack()
        self.ISpuleSR.pack()
        self.ISpuleEntry.pack()
        self.UBeschleunigungLabel.pack()
        self.UBeschleunigungScale.pack()
        self.UBeschleunigungEntry.pack()
        self.UKondensatorLabel.pack()
        self.UKondensatorScale.pack()
        self.UKondensatorEntry.pack()
        self.angleLabel.pack()
        self.angleScale.pack()
        self.elevationLabel.pack()
        self.elevationScale.pack()


if __name__ == "__main__":
    window = Window()
